# README by N. Sadawi 31/03/2014

This is a simple java application that tries to measure an average line thickness/width using the following procedure:
1- Assuming we have a binary image, we thin the image and save the resulting thinned image
2- To get good results, we need to make sure that the thinning is optimised, i.e. all unnecessary fg pixels should be turned into bg
3- We should trace all the thinned lines and save coordinates of fg pixels
4- The tracing is inspired by "Maze Traversal" algorithms
5- May be we split at multi-way junctions so we have several lines and/or poly-lines as this is useful for finding line endpoints
6- We assume that the thinning algorithm we have is a good one and the thin lines are in the middle of the originals
7- As we have coordinates of the thin lines, we can use these coordinates and use them as centres to draw circles on the original image
8- A circle drawing algirthm/code can be found here: http://tech-algorithm.com/articles/drawing-circle-explained/
9- What we need to do is, record all points on the circumference of the circle and make sure all of them are fg
10- We increase the radius and repeat step 9
11- We continue points 9 and 10 until one or more points are bg
12- We record the highest radius value
13- We repeat steps 9-12 for all coordinates of points on thin lines
14- Using the radius values we computed (multiply by 2 to have line width) we can compute average line width!

